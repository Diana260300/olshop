<?php
use App\Http\Controllers\PesanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('satu', function () {
    return view('profil');
});

Route::get('dua', function () {
    return view('detail');
});

Route::get('empat', function () {
    return view('kontak');
});
Route::get('/pesan', 'PesanController@index')->name('pesan');
Route::get('/tambahpesan', 'PesanController@create')->name('tambahpesan');
Route::post('/simpan', 'PesanController@store')->name('simpan');
Route::get('/edit/{id}',[PesanController::class,'edit'])->name('edit');
Route::put('/ubah/{id}',[PesanController::class,'update'])->name('ubah');
Route::get('/hapus/{id}',[PesanController::class,'destroy'])->name('hapus');

