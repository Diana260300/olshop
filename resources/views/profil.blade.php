<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <style>
      section {
        min-height: 420px;
      }
    </style>
        <title>Olshop</title>
      </head>
      <body class="mt-5">
    
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-success">
        <div class="container">
          <a class="navbar-brand nav-link active" href="/">Bestie Olshop</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link active" href="/">Home<span class="sr-only">(current)</span></a>
              <a class="nav-link active" href="satu">Profil Olshop</a>
              <a class="nav-link active" href="dua">Detail Produk</a>
              <a class="nav-link active" href="{{ route('pesan')}}">Pesanan</a>
              <a class="nav-link active" href="empat">Contact Us</a>
            </div>
          </div>
          </div>
        </nav>
        
        <div class="jumbotron jumbotron-fluid">
    
          <div class="container text-center">
          <img src="image/logoi.jpg" width="20%" class="rounded-circle img-thumbnail">
            <h1 class="display-4">BESTIE OLSHOP PAMEKASAN</h1>
            <p class="lead">Welcome To Our website and Happy Shopping. </p>
          </div>
        </div>
    
        <section id="about" class="about">
        <div class="container text-center">
          <div class="row mb-4">
            <div class="col">
              <h2 class="alert alert-primary text-center mt-3">OUR PROFILE</h2>
            </div>
          </div>
        </div>
    
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-5 text-center">
            <p> Online shop sekarang sudah menjadi trend penggunaan yang sering di gunakan oleh banyak kalangan, trend online shop ini membuat seseorang menjadi lebih mudah untuk melakukan transaksi jual beli, karena mereka tidak harus bersusah payah untuk menghadiri tokonya, cukup memanfaatkan handphone/smartphone anda untuk melakukan transaksi jual beli lalu pesanan anda segera dikirimkan, mudah sekali bukan? kemudahan berbelanja online sebagai akibat dari perkembangan zaman dapat kita rasakan. Bestie Olshop menjawab tantangan dari kemajuan zaman dalam bidang jual beli online.</p>
            </div>
            <div class="col-md-5 text-center">
              <p>BESTIE OLSHOP PAMEKASAN  adalah sebuah online shop yang didirikan oleh saya bersama sahabat kecil saya yang ingin memulai bisnis bersama, kami hadir sebagai salah satu toko online yang menjual beberapa produk best seller yang membantu melengkapi kebutuhan anda sekeluarga.mulai dari penjualan gamis best seller, scarlet whitening by fellycia Angelista dan beberapa produk cantik dari bella square, kami juga menyediakan barang-barang yang bisa di request selain dari pada itu. kepercayaan dari konsumen adalah penting untuk kami terus melayani kebutuhan anda semua, harapan kedepan adalah menjadi ujung tombak pemasarn produk.</p>
            </div>
          </div>
        </div>
        </section>
        <div class="alert alert-danger" role="alert">
          Thank's for visit Our Website enjoy your life with our best product <a href="{{ route('pesan') }}" class="{{ route('pesan') }}">Bestie olshop</a>. Give it a click if you like.
            </div>
     <footer class="bg-success text-white">
    <div class="container">
        <div class="col text-center">
          <p>Bestie Olshop   |  copyright 2021</p>
        </div>
      </div>
    </div>
  </footer>   
  <script>
    function msg()
    {
      email = document.getElementById("email").value;
      nama= document.getElementById("nama").value;
      alert ("Oke Thank You " + nama + " Happy Shopping Kaka.. ");
    }
  </script>   
  
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>