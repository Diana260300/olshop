<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- My CSS -->
    <style>
      section {
        min-height: 420px;
      }
    </style>
      <title>Olshop</title>
    </head>
    <body class="mt-5">

      <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-success">
      <div class="container">
        <a class="navbar-brand nav-link active" href="/">Bestie Olshop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-link active" href="/">Home<span class="sr-only">(current)</span></a>
            <a class="nav-link active" href="satu">Profil Olshop</a>
            <a class="nav-link active" href="dua">Detail Produk</a>
            <a class="nav-link active" href="{{ route('pesan')}}">Pesanan</a>
            <a class="nav-link active" href="empat">Contact Us</a>
          </div>
        </div>
        </div>
      </nav>
      
      <div class="jumbotron jumbotron-fluid">

        <div class="container text-center">
        <img src="image/logoi.jpg" width="20%" class="rounded-circle img-thumbnail">
          <h1 class="display-4">BESTIE OLSHOP PAMEKASAN</h1>
          <p class="lead">Welcome To Our website and Happy Shopping. </p>
        </div>
      </div>
      <div class="container text-center">
        <div class="row mb-4 pt-4">
          <div class="col">
            <h2 class="alert alert-primary text-center mt-3">CONTACT US</h2>
          </div>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-lg-4 mb-4">
          <div class="card text-white bg-success mb-3 text-center">
            <div class="card-body">
              <h5 class="card-title">Contact Us</h5>
              <p class="card-text">Silahkan Hubungi Kami Melalui Website Yang Tersedia</p>
            </div>
          </div>
          <ul class="list-group">
            <li class="list-group-item"><h1>Our Location</h1></li>
            <li class="list-group-item ">BESTIE OLSHOP</li>
            <li class="list-group-item">Larangan Luar, Larangan Pamekasan</li>
            <li class="list-group-item">East Java, Indonesia</li>
          </ul>
        </div> 
      
      <div class="col-lg-6">
        <form>
          <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" placeholder="masukkan nama lengkap anda">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" placeholder="masukkan email anda">
          </div>
          <div class="form-group">
            <label for="telp">No. Telepon</label>
            <input type="text" class="form-control" id="telp">
          </div>
          <div class="form-group">
            <label for="pesan">Pesan</label>
            <textarea name="pesan" id="pesan" class="form-control"></textarea>
          </div>
            <input type="submit" class="btn btn-primary" name="simpan"value="simpan" onclick="msg()">
          </div>
        </form>
      </div>
    </div><br>
    <div class="alert alert-danger" role="alert">
      Thank's for visit Our Website enjoy your life with our best product <a href="{{ route('pesan') }}" class="{{ route('pesan') }}">Bestie olshop</a>. Give it a click if you like.
        </div>

      <footer class="bg-success text-white">
        <div class="container">
            <div class="col text-center">
              <p>Bestie Olshop  |  copyright 2021</p>
            </div>
          </div>
        </div>
      </footer>
      <script>
        function msg()
        {
          email = document.getElementById("email").value;
          nama= document.getElementById("nama").value;
          alert ("Oke Thank You " + nama + " Happy Shopping Kaka.. ");
        }
      </script>
        
    
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
  </html>