<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <style>
      section {
        min-height: 420px;
      }
    </style>

    <title>Olshop</title>
  </head>
  <body class="mt-5">

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-success">
    <div class="container">
      <a class="navbar-brand nav-link active" href="/">Bestie Olshop</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-link active" href="/">Home<span class="sr-only">(current)</span></a>
          <a class="nav-link active" href="satu">Profil Olshop</a>
          <a class="nav-link active" href="dua">Detail Produk</a>
          <a class="nav-link active" href="{{ route('pesan')}}">Pesanan</a>
          <a class="nav-link active" href="empat">Contact Us</a>
        </div>
      </div>
      </div>
    </nav>
    
    <div class="jumbotron jumbotron-fluid">

      <div class="container text-center">
      <img src="image/logoi.jpg" width="20%" class="rounded-circle img-thumbnail">
        <h1 class="display-4">BESTIE OLSHOP PAMEKASAN</h1>
        <p class="lead">Welcome To Our website and Happy Shopping. </p>
      </div>
    </div>
        <section>
            <div class="container text-center">
                <h2 class="alert alert-primary text-center mt-3">PESANAN ANDA</h2>
                <div class="content">
                    <div class="card card-info card-outline">
                        <div class="card-header">
                            <div class="card-tools">
                                <a href="{{ route('tambahpesan')}}" class="btn btn-success">Tambah data<i class="fas fa-plus-square"></i></a>
                            </div>
                        </div>

                        <div class="card-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Nama Barang</th>
                                    <th>Jenis Barang</th>
                                    <th>Pilihan Harga Jual</th>
                                    <th>Jumlah Pesanan</th>
                                    <th>Tanggal Pengiriman</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                                @foreach ($dtpesanan as $item)
                                <tr>
                                    <td>{{$item->nama_barang}}</td>
                                    <td>{{$item->jenis_barang}}</td>
                                    <td>{{$item->pilihan_harga_jual}}</td>
                                    <td>{{$item->jumlah_pesanan}}</td>
                                    <td>{{date ('d-m-Y', strtotime ($item->tanggal_pengiriman))}}</td>
                                    <td>{{$item->keterangan}}</td>
                                    <td>
                                      <a href="{{ route('edit',$item->id)}}"><button> edit</button></a> 
                                       <a href="{{ url('hapus',$item->id)}}"><button> hapus</button></a>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
            <footer class="bg-success text-white">
                <div class="container">
                    <div class="col text-center">
                      <p>Bestie Olshop   |  copyright 2021</p>
                    </div>
                  </div>
                </div>
              </footer>      
              
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    </body>
</html>