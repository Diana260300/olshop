<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- My CSS -->
    <style>
      section {
        min-height: 420px;
      }
    </style>

    <title>Olshop</title>
  </head>
  <body class="mt-5">

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-success">
    <div class="container">
      <a class="navbar-brand nav-link active" href="/">Bestie Olshop</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-link active" href="/">Home<span class="sr-only">(current)</span></a>
          <a class="nav-link active" href="satu">Profil Olshop</a>
          <a class="nav-link active" href="dua">Detail Product</a>
          <a class="nav-link active" href="{{ route('pesan') }}">Pesanan</a>
          <a class="nav-link active" href="empat">Contact Us</a>
        </div>
      </div>
      </div>
    </nav>
    
    <div class="jumbotron jumbotron-fluid">

      <div class="container text-center">
      <img src="image/logoi.jpg" width="20%" class="rounded-circle img-thumbnail">
        <h1 class="display-4">BESTIE OLSHOP PAMEKASAN</h1>
        <p class="lead">Welcome To Our website and Happy Shopping. </p>
      </div>
    </div>

    <section id="about" class="about">
    <div class="container text-center">
      <div class="row mb-4">
        <div class="col">
          <h1>Our Profile</h1>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-5 text-center">
        <p> Online shop sekarang sudah menjadi trend penggunaan yang sering di gunakan oleh banyak kalangan, trend online shop ini membuat seseorang menjadi lebih mudah untuk melakukan transaksi jual beli, karena mereka tidak harus bersusah payah untuk menghadiri tokonya, cukup memanfaatkan handphone/smartphone anda untuk melakukan transaksi jual beli lalu pesanan anda segera dikirimkan, mudah sekali bukan? kemudahan berbelanja online sebagai akibat dari perkembangan zaman dapat kita rasakan. Bestie Olshop menjawab tantangan dari kemajuan zaman dalam bidang jual beli online.</p>
        </div>
        <div class="col-md-5 text-center">
          <p>BESTIE OLSHOP PAMEKASAN  adalah sebuah online shop yang didirikan oleh saya bersama sahabat kecil saya yang ingin memulai bisnis bersama, kami hadir sebagai salah satu toko online yang menjual beberapa produk best seller yang membantu melengkapi kebutuhan anda sekeluarga.mulai dari penjualan gamis best seller, scarlet whitening by fellycia Angelista dan beberapa produk cantik dari bella square, kami juga menyediakan barang-barang yang bisa di request selain dari pada itu. kepercayaan dari konsumen adalah penting untuk kami terus melayani kebutuhan anda semua, harapan kedepan adalah menjadi ujung tombak pemasarn produk.</p>
        </div>
      </div>
    </div>
    </section>

    <section id="Detail Produk " class="Detail Produk bg-light pb-4">
    <div class="container text-center bg-light">
      <div class="row mb-4 pt-4">
        <div class="col">
          <h1>Detail Product</h1>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-md">
          <div class="card">
            <img src="image/reina.jpg" class="card-img-top" alt="...">
            <div class="card-body">
              <p class="card-text">Rheina Basic Dres by NARARYA </p>
            </div>
            <div class="card-body">
              <p class="card-text">dengan matterial Lady Zara (import),bahan tebal, anyep dan super flowly, streat 4 sisi dengan anyaman serat woll yang halus dan kerapatan yang kecil. 
                Detail Baju: Busui sleting, ukuran= L-XXL, Aksen Renda pada rok sehingga terlihat ramping, Tali pinggang yang fleksibel sehingga bisa di sesuaikan dengan bentuk tubuh, Renda tangan yang mempermanis tampilan, Lebar rok 3m. dihiasi dengan renda rajut dan remple.</p>
            </div>
            <div class="card-body">
              <p class="card-text">Nb: List orderan rheina bisa di setor setiap hari ya... Happy Shopping.  </p>
            </div>
          </div>
        </div>
          <div class="col-md">
            <div class="card">
              <img src="image/scarlet.jpg" class="card-img-top" alt="...">
              <div class="card-body">
                <p class="card-text">Scarlett adalah produk body care atau perawatan tubuh yang dirilis oleh artis Indonesia, Felicya Angelista dengan produk yang berfokus pada pencerah kulit. Scarlett Whitening ini juga bermanfaat untuk mengangkat sel kulit mati dan kotoran yang ada di tubuh kita. Berikut selengkapnya:</p>
              </div>
              <div class="card-body">
                <p class="card-text">Produk dari Scarlett umumnya mengandung bahan aktif Glutathione (vitamin pencerah kulit). Glutathione sendiri adalah master antioksidan terbaik yang sudah terbukti paling ampuh dan cepat untuk mencerahkan kulit dengan penggunaan rutin.
                  Selain sebagai antioksidan, produk ini juga dapat membantu menangkal radikal bebas serta menutrisi kulit kita.
                  Produk ini juga sudah lolos uji BPOM dan juga halal.
                  Scarlett Whitening juga terbukti aman untuk diaplikasikan ke tubuh kita, juga nyaman untuk semua jenis kulit.
                  Aroma dari body care ini terasa sangat menyegarkan dan tahan lama sehingga tubuh terasa segar sepanjang hari</p>
              </div>
            </div>
          </div>
            <div class="col-md">
              <div class="card">
                <img src="image/bella square.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <p class="card-text">Detail Bahan Kerudung Bella Square Segiempat</p>
                </div>
                <div class="card-body">
                  <p class="card-text">Tepian neci rapi, Bahan double hycon, Ukuran segi empat 115 x 115,Tersedia 25 pilihan warna berbeda.</p>
                </div>
                <div class="card-body">
                  <p class="card-text">Hijab Bella Square memiliki kelebihan di antaranya, bahannya lembut dan tidak menerawang; mudah diatur dan dibentuk; serta tidak panas dan nyaman dipakai. Menurut sejumlah review pengguna di internet mengungkapkan, kerudung segiempat Bella Square begitu mudah dibentuk dan nyaman digunakan. Pilihan warna yang banyak menjadi daya tarik tersendiri bagi konsumen. Selain itu, harganya yang murah membuatnya banyak dicari konsumen, khususnya bagi para pelajar.</p>
                </div>
              </div>
            </div>
            <div class="col-md">
              <div class="card">
                <img src="image/pashmina.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <p class="card-text">Jilbab diamond adalah jenis kerudung yang dibuat dari bahan kain diamond. Kain diamond ini memiliki tekstur seperti kulit jeruk. Berikut adalah jenis-jenis kain diamond yang biasa digunakan untuk kerudung:</p>
                </div>
                <div class="card-body">
                  <p class="card-text">    Jilbab Diamond Cerutti

                    Kain diamond cerutti biasa juga dikenal dengan diamond crepe atau georgette. Kain diamond ini paling tipis dibandingkan yang lain, tetapi memiliki tekstur paling harus. Biasanya diamond cerutti digunakan untuk membuat kerudung instan syar’i dan khimar yang memerlukan bahan lembut dan terkesan jatuh.</p>
                </div>
                <div class="card-body">
                  <p class="card-text">    Jilbab Diamond Italiano

                    Kain diamond italiano adalah yang paling tebal. Kain diamond italiano sering digunakan untuk bahan pashmina atau jilbab segi empat diamond.</p>
                </div>
              </div>
            </div>
      </div>
    </div>
    </section>

    <div class="container text-center">
      <div class="row mb-4 pt-4">
        <div class="col">
          <h1>Contact Us</h1>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-lg-4 mb-4">
        <div class="card text-white bg-success mb-3 text-center">
          <div class="card-body">
            <h5 class="card-title">Contact Us</h5>
            <p class="card-text">Silahkan Hubungi Kami Melalui Website Yang Tersedia</p>
          </div>
        </div>
        <ul class="list-group">
          <li class="list-group-item"><h1>Our Location</h1></li>
          <li class="list-group-item ">BESTIE OLSHOP</li>
          <li class="list-group-item">Larangan Luar, Larangan Pamekasan</li>
          <li class="list-group-item">East Java, Indonesia</li>
        </ul>
      </div> 
    
    <div class="col-lg-6">
      <form>
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" placeholder="masukkan nama lengkap anda">
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text" class="form-control" id="email" placeholder="masukkan email anda">
        </div>
        <div class="form-group">
          <label for="telp">No. Telepon</label>
          <input type="text" class="form-control" id="telp">
        </div>
        <div class="form-group">
          <label for="pesan">Pesan</label>
          <textarea name="pesan" id="pesan" class="form-control"></textarea>
        </div>
      </div>
      <input type="submit" class="btn btn-primary" name="simpan"value="simpan" onclick="msg()"></br>
    </div>
      </form>
    </div>
  </div><br>
  <div class="alert alert-danger" role="alert">
Thank's for visit Our Website enjoy your life with our best product <a href="{{ route('pesan') }}" class="{{ route('pesan') }}">Bestie olshop</a>. Give it a click if you like.
  </div>

    <footer class="bg-success text-white">
      <div class="container">
          <div class="col text-center">
            <p>Bestie Olshop  |  copyright 2021</p>
          </div>
        </div>
    </footer>
    <script>
      function msg()
      {
        email = document.getElementById("email").value;
        nama= document.getElementById("nama").value;
        alert ("Oke Thank You " + nama + " Happy Shopping Kaka.. ");
      }
    </script>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>