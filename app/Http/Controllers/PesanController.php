<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pesanan; 

class PesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dtpesanan = pesanan::all();
        return view('pesan',compact('dtpesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahpesan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd ($request->all());
        pesanan::create([
            'nama_barang' => $request->nama,
            'jenis_barang' => $request->jenis,
            'pilihan_harga_jual' => $request->harga,
            'jumlah_pesanan' => $request->jumlah,
            'tanggal_pengiriman' => $request->tglpsn,
            'keterangan' => $request->keterangan,
        ]);
        return redirect('pesan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = pesanan::find($id);
        return view('edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = pesanan::findorfail($id);
        $save= $data->update([
            'nama_barang' => $request->nama,
            'jenis_barang' => $request->jenis,
            'pilihan_harga_jual' => $request->harga,
            'jumlah_pesanan' => $request->jumlah,
            'tanggal_pengiriman' => $request->tglpsn,
            'keterangan' => $request->keterangan,
        ]);
        if($save){
            $dtpesanan = pesanan::all();
            return redirect()->route('pesan', compact('dtpesanan'));
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pes = pesanan::findorfail($id);
        $pes->delete();
        return back();
    }
}
