<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pesanan extends Model
{
    protected $table ="pesanans";
    protected $primaryKey="id";
    protected $fillable = [
        'id','nama_barang','jenis_barang','pilihan_harga_jual','jumlah_pesanan','tanggal_pengiriman','keterangan'];
}
